Rscript 00_run.R \
  input/expr/MG63_DAn48_mtx_gene_count_mv3.gene.tsv.gz \
  input/info/library_info.test_1.tsv \
  input/info/gene_names_2.tsv \
  input/info/c2.cp.v6.0.symbols.gmt \
  20 \
  500 \
  50 \
  output/PCA_t1.pdf \
  output/BCV_t1.pdf \
  output/size.factor_t1.txt \
  output/MA.plot_t1.pdf \
  output/volcano.plot_t1.pdf \
  output/DEGNum_t1.txt \
  output/glmQLFTest_result_t1.tsv \
  output/fgsea.results_t1.tsv \
  output/fgsea.results_t1.pdf \
  output/normailized_cpm_t1.tsv.txt 

